/// <reference types="react" />
export interface TableProps {
    heading: object;
    data: object;
}
export declare const Table: ({}: TableProps) => JSX.Element;
