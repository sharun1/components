/// <reference types="react" />
export interface TablewithDynamicRowsProps {
    data: object;
}
export declare const TablewithDynamicRows: ({}: TablewithDynamicRowsProps) => JSX.Element;
