/// <reference types="react" />
export interface InputfieldProps {
    /**
     * we have different varient(versions) of input
     */
    varient: string;
    fullWidth: boolean;
    label: string;
    color: string;
    size: string;
    disabled?: boolean;
    onChange?: () => {};
    placeholder: string;
    type: string;
    formHelp: string;
    error?: boolean;
    iconPosition?: string;
    iconName?: any;
}
/**
 * this component will be used a input html tag but with custom features
 */
export declare const Inputfield: ({ varient, fullWidth, label, disabled, formHelp, placeholder, type, size, error, onChange, iconPosition, iconName }: InputfieldProps) => JSX.Element;
