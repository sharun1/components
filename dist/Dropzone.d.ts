/// <reference types="react" />
export interface DropzoneProps {
    onDrop: Function;
    showFiles?: boolean;
}
export declare const Dropzone: ({}: DropzoneProps) => JSX.Element;
