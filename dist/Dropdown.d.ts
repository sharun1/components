/// <reference types="react" />
export interface DropdownProps {
    color: string;
    placeHolder?: string;
    onChange?: () => {};
    size: string;
    disabled?: boolean;
    options: any;
    defaultOption?: string;
}
export declare const Dropdown: ({ options, disabled, color, size, placeHolder, onChange, ...props }: DropdownProps) => JSX.Element;
