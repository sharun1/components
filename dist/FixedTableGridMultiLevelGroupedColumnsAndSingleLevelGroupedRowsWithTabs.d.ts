/// <reference types="react" />
export interface FixedTableGridMultiLevelGroupedColumnsAndSingleLevelGroupedRowsWithTabsProps {
    data: object;
}
export declare const FixedTableGridMultiLevelGroupedColumnsAndSingleLevelGroupedRowsWithTabs: ({}: FixedTableGridMultiLevelGroupedColumnsAndSingleLevelGroupedRowsWithTabsProps) => JSX.Element;
