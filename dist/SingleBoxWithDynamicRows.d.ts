/// <reference types="react" />
export interface SingleBoxWithDynamicRowsProps {
    data: object;
}
export declare const SingleBoxWithDynamicRows: ({}: SingleBoxWithDynamicRowsProps) => JSX.Element;
