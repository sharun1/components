'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = require('react');
var React__default = _interopDefault(React);
var classname = _interopDefault(require('classnames'));
var Datasheet = _interopDefault(require('react-datasheet'));

function _regeneratorRuntime() {
  /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */

  _regeneratorRuntime = function () {
    return exports;
  };

  var exports = {},
      Op = Object.prototype,
      hasOwn = Op.hasOwnProperty,
      $Symbol = "function" == typeof Symbol ? Symbol : {},
      iteratorSymbol = $Symbol.iterator || "@@iterator",
      asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator",
      toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function define(obj, key, value) {
    return Object.defineProperty(obj, key, {
      value: value,
      enumerable: !0,
      configurable: !0,
      writable: !0
    }), obj[key];
  }

  try {
    define({}, "");
  } catch (err) {
    define = function (obj, key, value) {
      return obj[key] = value;
    };
  }

  function wrap(innerFn, outerFn, self, tryLocsList) {
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator,
        generator = Object.create(protoGenerator.prototype),
        context = new Context(tryLocsList || []);
    return generator._invoke = function (innerFn, self, context) {
      var state = "suspendedStart";
      return function (method, arg) {
        if ("executing" === state) throw new Error("Generator is already running");

        if ("completed" === state) {
          if ("throw" === method) throw arg;
          return doneResult();
        }

        for (context.method = method, context.arg = arg;;) {
          var delegate = context.delegate;

          if (delegate) {
            var delegateResult = maybeInvokeDelegate(delegate, context);

            if (delegateResult) {
              if (delegateResult === ContinueSentinel) continue;
              return delegateResult;
            }
          }

          if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) {
            if ("suspendedStart" === state) throw state = "completed", context.arg;
            context.dispatchException(context.arg);
          } else "return" === context.method && context.abrupt("return", context.arg);
          state = "executing";
          var record = tryCatch(innerFn, self, context);

          if ("normal" === record.type) {
            if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue;
            return {
              value: record.arg,
              done: context.done
            };
          }

          "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg);
        }
      };
    }(innerFn, self, context), generator;
  }

  function tryCatch(fn, obj, arg) {
    try {
      return {
        type: "normal",
        arg: fn.call(obj, arg)
      };
    } catch (err) {
      return {
        type: "throw",
        arg: err
      };
    }
  }

  exports.wrap = wrap;
  var ContinueSentinel = {};

  function Generator() {}

  function GeneratorFunction() {}

  function GeneratorFunctionPrototype() {}

  var IteratorPrototype = {};
  define(IteratorPrototype, iteratorSymbol, function () {
    return this;
  });
  var getProto = Object.getPrototypeOf,
      NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype);
  var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype);

  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function (method) {
      define(prototype, method, function (arg) {
        return this._invoke(method, arg);
      });
    });
  }

  function AsyncIterator(generator, PromiseImpl) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);

      if ("throw" !== record.type) {
        var result = record.arg,
            value = result.value;
        return value && "object" == typeof value && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) {
          invoke("next", value, resolve, reject);
        }, function (err) {
          invoke("throw", err, resolve, reject);
        }) : PromiseImpl.resolve(value).then(function (unwrapped) {
          result.value = unwrapped, resolve(result);
        }, function (error) {
          return invoke("throw", error, resolve, reject);
        });
      }

      reject(record.arg);
    }

    var previousPromise;

    this._invoke = function (method, arg) {
      function callInvokeWithMethodAndArg() {
        return new PromiseImpl(function (resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg();
    };
  }

  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];

    if (undefined === method) {
      if (context.delegate = null, "throw" === context.method) {
        if (delegate.iterator.return && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method)) return ContinueSentinel;
        context.method = "throw", context.arg = new TypeError("The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);
    if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel;
    var info = record.arg;
    return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel);
  }

  function pushTryEntry(locs) {
    var entry = {
      tryLoc: locs[0]
    };
    1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal", delete record.arg, entry.completion = record;
  }

  function Context(tryLocsList) {
    this.tryEntries = [{
      tryLoc: "root"
    }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0);
  }

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) return iteratorMethod.call(iterable);
      if ("function" == typeof iterable.next) return iterable;

      if (!isNaN(iterable.length)) {
        var i = -1,
            next = function next() {
          for (; ++i < iterable.length;) if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next;

          return next.value = undefined, next.done = !0, next;
        };

        return next.next = next;
      }
    }

    return {
      next: doneResult
    };
  }

  function doneResult() {
    return {
      value: undefined,
      done: !0
    };
  }

  return GeneratorFunction.prototype = GeneratorFunctionPrototype, define(Gp, "constructor", GeneratorFunctionPrototype), define(GeneratorFunctionPrototype, "constructor", GeneratorFunction), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) {
    var ctor = "function" == typeof genFun && genFun.constructor;
    return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name));
  }, exports.mark = function (genFun) {
    return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun;
  }, exports.awrap = function (arg) {
    return {
      __await: arg
    };
  }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () {
    return this;
  }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) {
    void 0 === PromiseImpl && (PromiseImpl = Promise);
    var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl);
    return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) {
      return result.done ? result.value : iter.next();
    });
  }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () {
    return this;
  }), define(Gp, "toString", function () {
    return "[object Generator]";
  }), exports.keys = function (object) {
    var keys = [];

    for (var key in object) keys.push(key);

    return keys.reverse(), function next() {
      for (; keys.length;) {
        var key = keys.pop();
        if (key in object) return next.value = key, next.done = !1, next;
      }

      return next.done = !0, next;
    };
  }, exports.values = values, Context.prototype = {
    constructor: Context,
    reset: function (skipTempReset) {
      if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined);
    },
    stop: function () {
      this.done = !0;
      var rootRecord = this.tryEntries[0].completion;
      if ("throw" === rootRecord.type) throw rootRecord.arg;
      return this.rval;
    },
    dispatchException: function (exception) {
      if (this.done) throw exception;
      var context = this;

      function handle(loc, caught) {
        return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i],
            record = entry.completion;
        if ("root" === entry.tryLoc) return handle("end");

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc"),
              hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0);
            if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc);
          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0);
          } else {
            if (!hasFinally) throw new Error("try statement without catch or finally");
            if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc);
          }
        }
      }
    },
    abrupt: function (type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];

        if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null);
      var record = finallyEntry ? finallyEntry.completion : {};
      return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record);
    },
    complete: function (record, afterLoc) {
      if ("throw" === record.type) throw record.arg;
      return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel;
    },
    finish: function (finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel;
      }
    },
    catch: function (tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];

        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;

          if ("throw" === record.type) {
            var thrown = record.arg;
            resetTryEntry(entry);
          }

          return thrown;
        }
      }

      throw new Error("illegal catch attempt");
    },
    delegateYield: function (iterable, resultName, nextLoc) {
      return this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      }, "next" === this.method && (this.arg = undefined), ContinueSentinel;
    }
  }, exports;
}

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}

function _objectDestructuringEmpty(obj) {
  if (obj == null) throw new TypeError("Cannot destructure undefined");
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

var _excluded = ["width", "height", "fill", "fillOpacity"];
var Eye = function Eye(_ref) {
  var width = _ref.width,
      height = _ref.height,
      fill = _ref.fill,
      fillOpacity = _ref.fillOpacity,
      props = _objectWithoutPropertiesLoose(_ref, _excluded);

  return /*#__PURE__*/React__default.createElement(React__default.Fragment, null, /*#__PURE__*/React__default.createElement("svg", Object.assign({
    width: width,
    height: height,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), /*#__PURE__*/React__default.createElement("path", {
    d: "M12 4.5C7 4.5 2.73 7.61 1 12c1.73 4.39 6 7.5 11 7.5s9.27-3.11 11-7.5c-1.73-4.39-6-7.5-11-7.5ZM12 17c-2.76 0-5-2.24-5-5s2.24-5 5-5 5 2.24 5 5-2.24 5-5 5Zm0-8c-1.66 0-3 1.34-3 3s1.34 3 3 3 3-1.34 3-3-1.34-3-3-3Z",
    fill: fill,
    fillOpacity: fillOpacity
  })));
};

var _excluded$1 = ["width", "height", "fill", "fillOpacity"];
var ArrowLeft = function ArrowLeft(_ref) {
  var width = _ref.width,
      height = _ref.height,
      fill = _ref.fill,
      fillOpacity = _ref.fillOpacity,
      props = _objectWithoutPropertiesLoose(_ref, _excluded$1);

  return /*#__PURE__*/React__default.createElement("svg", Object.assign({
    width: width,
    height: height,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), /*#__PURE__*/React__default.createElement("path", {
    d: "M5 13h11.17l-4.88 4.88c-.39.39-.39 1.03 0 1.42.39.39 1.02.39 1.41 0l6.59-6.59a.996.996 0 0 0 0-1.41l-6.58-6.6a.996.996 0 1 0-1.41 1.41L16.17 11H5c-.55 0-1 .45-1 1s.45 1 1 1Z",
    fill: fill,
    fillOpacity: fillOpacity
  }));
};

var _excluded$2 = ["width", "height", "fill", "fillOpacity"];
var CloseEye = function CloseEye(_ref) {
  var _ref$width = _ref.width,
      width = _ref$width === void 0 ? "24" : _ref$width,
      _ref$height = _ref.height,
      height = _ref$height === void 0 ? "24" : _ref$height,
      _ref$fill = _ref.fill,
      fill = _ref$fill === void 0 ? "#000000" : _ref$fill,
      fillOpacity = _ref.fillOpacity,
      props = _objectWithoutPropertiesLoose(_ref, _excluded$2);

  return /*#__PURE__*/React__default.createElement(React__default.Fragment, null, /*#__PURE__*/React__default.createElement("svg", Object.assign({
    width: width,
    height: height,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), /*#__PURE__*/React__default.createElement("path", {
    d: "M12 6.5c2.76 0 5 2.24 5 5 0 .51-.1 1-.24 1.46l3.06 3.06c1.39-1.23 2.49-2.77 3.18-4.53C21.27 7.11 17 4 12 4c-1.27 0-2.49.2-3.64.57l2.17 2.17c.47-.14.96-.24 1.47-.24ZM2.71 3.16a.996.996 0 0 0 0 1.41l1.97 1.97A11.892 11.892 0 0 0 1 11.5C2.73 15.89 7 19 12 19c1.52 0 2.97-.3 4.31-.82l2.72 2.72a.996.996 0 1 0 1.41-1.41L4.13 3.16c-.39-.39-1.03-.39-1.42 0ZM12 16.5c-2.76 0-5-2.24-5-5 0-.77.18-1.5.49-2.14l1.57 1.57c-.03.18-.06.37-.06.57 0 1.66 1.34 3 3 3 .2 0 .38-.03.57-.07L14.14 16c-.65.32-1.37.5-2.14.5Zm2.97-5.33a2.97 2.97 0 0 0-2.64-2.64l2.64 2.64Z",
    fill: fill,
    fillOpacity: fillOpacity
  })));
};

var _excluded$3 = ["width", "height", "fill", "fillOpacity"];
var Pencil = function Pencil(_ref) {
  var width = _ref.width,
      height = _ref.height,
      fill = _ref.fill,
      fillOpacity = _ref.fillOpacity,
      props = _objectWithoutPropertiesLoose(_ref, _excluded$3);

  return /*#__PURE__*/React__default.createElement("svg", Object.assign({
    width: width,
    height: height,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), /*#__PURE__*/React__default.createElement("path", {
    d: "M3 17.46v3.04c0 .28.22.5.5.5h3.04c.13 0 .26-.05.35-.15L17.81 9.94l-3.75-3.75L3.15 17.1c-.1.1-.15.22-.15.36ZM20.71 7.04a.996.996 0 0 0 0-1.41l-2.34-2.34a.996.996 0 0 0-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83Z",
    fill: fill,
    fillOpacity: fillOpacity
  }));
};

var _excluded$4 = ["width", "height", "fill", "fillOpacity"];
var Delete = function Delete(_ref) {
  var width = _ref.width,
      height = _ref.height,
      fill = _ref.fill,
      fillOpacity = _ref.fillOpacity,
      props = _objectWithoutPropertiesLoose(_ref, _excluded$4);

  return /*#__PURE__*/React__default.createElement("svg", Object.assign({
    width: width,
    height: height,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), /*#__PURE__*/React__default.createElement("path", {
    d: "M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V9c0-1.1-.9-2-2-2H8c-1.1 0-2 .9-2 2v10ZM18 4h-2.5l-.71-.71c-.18-.18-.44-.29-.7-.29H9.91c-.26 0-.52.11-.7.29L8.5 4H6c-.55 0-1 .45-1 1s.45 1 1 1h12c.55 0 1-.45 1-1s-.45-1-1-1Z",
    fill: fill,
    fillOpacity: fillOpacity
  }));
};

var _excluded$5 = ["width", "height", "fill", "fillOpacity"];
var Plus = function Plus(_ref) {
  var width = _ref.width,
      height = _ref.height,
      fill = _ref.fill,
      fillOpacity = _ref.fillOpacity,
      props = _objectWithoutPropertiesLoose(_ref, _excluded$5);

  return /*#__PURE__*/React__default.createElement("svg", Object.assign({
    width: width,
    height: height,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), /*#__PURE__*/React__default.createElement("path", {
    d: "M18 13h-5v5c0 .55-.45 1-1 1s-1-.45-1-1v-5H6c-.55 0-1-.45-1-1s.45-1 1-1h5V6c0-.55.45-1 1-1s1 .45 1 1v5h5c.55 0 1 .45 1 1s-.45 1-1 1Z",
    fill: fill,
    fillOpacity: fillOpacity
  }));
};

var _excluded$6 = ["width", "height", "fill", "name", "fillOpacity", "onClickIcon"];
var Icons = {
  'eye': Eye,
  'arrow-left': ArrowLeft,
  'close-eye': CloseEye,
  'pencil': Pencil,
  'delete': Delete,
  'plus': Plus
};
var Icon = function Icon(_ref) {
  var _ref$width = _ref.width,
      width = _ref$width === void 0 ? "24" : _ref$width,
      _ref$height = _ref.height,
      height = _ref$height === void 0 ? "24" : _ref$height,
      _ref$fill = _ref.fill,
      fill = _ref$fill === void 0 ? "#000000" : _ref$fill,
      name = _ref.name,
      _ref$fillOpacity = _ref.fillOpacity,
      fillOpacity = _ref$fillOpacity === void 0 ? 0.54 : _ref$fillOpacity,
      onClickIcon = _ref.onClickIcon,
      props = _objectWithoutPropertiesLoose(_ref, _excluded$6);

  if (Icons[name] === undefined) return null;
  var IconComponent = Icons[name];
  return /*#__PURE__*/React__default.createElement(React__default.Fragment, null, /*#__PURE__*/React__default.createElement("span", {
    onClick: onClickIcon,
    className: classname('cursor-pointer')
  }, /*#__PURE__*/React__default.createElement(IconComponent, Object.assign({
    width: width,
    height: height,
    fill: fill,
    fillOpacity: fillOpacity
  }, props))));
};

var _excluded$7 = ["textColor", "label", "color", "size", "variant", "disabled", "iconPosition", "height", "width", "iconName", "iconColor", "onClick", "iconColorOpacity"];
// }

var Button = function Button(_ref) {
  var textColor = _ref.textColor,
      label = _ref.label,
      color = _ref.color,
      size = _ref.size,
      variant = _ref.variant,
      disabled = _ref.disabled,
      iconPosition = _ref.iconPosition,
      height = _ref.height,
      width = _ref.width,
      iconName = _ref.iconName,
      iconColor = _ref.iconColor,
      onClick = _ref.onClick,
      _ref$iconColorOpacity = _ref.iconColorOpacity,
      iconColorOpacity = _ref$iconColorOpacity === void 0 ? 1 : _ref$iconColorOpacity,
      props = _objectWithoutPropertiesLoose(_ref, _excluded$7);

  var variantClass;
  var sizeClass;
  var colorClass;

  if (variant) {
    switch (variant) {
      case 'contained':
        variantClass = "text-white border text-white font-bold";
        break;

      case 'outlined':
        variantClass = "bg-transparent font-semibold border border-2";
        colorClass = "text-" + textColor + " border-" + color + " hover:bg-" + color;
        break;

      case 'text':
        colorClass = "text-" + color;
        break;
    }
  }

  if (size) {
    switch (size) {
      case 'extrasmall':
        sizeClass = "py-1 px-2";
        break;

      case 'small':
        sizeClass = "py-1 px-4";
        break;

      case 'medium':
        sizeClass = "py-2 px-4";
        break;

      case 'large':
        sizeClass = "py-3 px-6";
        break;
    }
  }

  if (color) {
    switch (color) {
      case 'primary':
        if (variant === 'contained') {
          colorClass = 'bg-primary hover:shadow-primary';
        } else if (variant === 'outlined') {
          colorClass = "text-" + textColor + " border-primary hover:shadow-primary";
        }

        break;

      case 'secondary':
        if (variant === 'contained') {
          colorClass = 'bg-secondary hover:shadow-secondary';
        } else if (variant === 'outlined') {
          colorClass = "text-" + textColor + " border-secondary hover:shadow-secondary";
        }

        break;

      case 'success':
        if (variant === 'contained') {
          colorClass = 'bg-success hover:shadow-success';
        } else if (variant === 'outlined') {
          colorClass = "text-" + textColor + " border-success hover:shadow-success";
        }

        break;

      case 'normal':
        if (variant === 'contained') {
          colorClass = 'bg-normal hover:shadow-normal';
        } else if (variant === 'outlined') {
          colorClass = "text-" + textColor + " border-normal hover:shadow-normal";
        }

        break;

      case 'primary2':
        if (variant === 'contained') {
          colorClass = 'bg-primary2 hover:shadow-primary2';
        } else if (variant === 'outlined') {
          colorClass = "text-" + textColor + " border-primary2 hover:shadow-primary2";
        }

        break;

      case 'primary3':
        if (variant === 'contained') {
          colorClass = 'bg-primary3 hover:shadow-primary3';
        } else if (variant === 'outlined') {
          colorClass = "text-" + textColor + " border-primary3 hover:shadow-primary3";
        }

        break;

      case 'text':
        if (variant === 'contained') {
          colorClass = 'bg-text hover:shadow-text';
        } else if (variant === 'outlined') {
          colorClass = "text-" + textColor + " border-text hover:shadow-text";
        }

        break;

      case 'card':
        if (variant === 'contained') {
          colorClass = 'bg-card hover:shadow-card';
        } else if (variant === 'outlined') {
          colorClass = "text-" + textColor + " border-card hover:shadow-card";
        }

        break;

      default:
        colorClass = 'bg-primary';
        break;
    }
  }

  return /*#__PURE__*/React__default.createElement(React__default.Fragment, null, /*#__PURE__*/React__default.createElement("button", Object.assign({
    type: 'button',
    disabled: disabled,
    className: classname(sizeClass, variantClass, "rounded", 'min-w-[64dp]', 'min-h-[36dp]', 'hover:shadow', 'flex', 'justify-center', colorClass, width, height),
    onClick: onClick
  }, props), /*#__PURE__*/React__default.createElement("span", {
    className: 'mr-2'
  }, iconPosition === 'left' && /*#__PURE__*/React__default.createElement(Icon, {
    width: "24",
    height: "24",
    fill: iconColor,
    name: iconName,
    fillOpacity: iconColorOpacity
  })), label, /*#__PURE__*/React__default.createElement("span", {
    className: 'ml-2'
  }, iconPosition === 'right' && /*#__PURE__*/React__default.createElement(Icon, {
    width: "24",
    height: "24",
    fill: iconColor,
    name: iconName,
    fillOpacity: iconColorOpacity
  }))));
};

var Card = function Card(_ref) {
  var heading = _ref.heading,
      elevation = _ref.elevation,
      children = _ref.children,
      actionButton = _ref.actionButton;
  var shadow;

  if (elevation) {
    switch (elevation) {
      case 0:
        shadow = 'shadow-none';
        break;

      case 2:
        shadow = 'shadow-sm';
        break;

      case 4:
        shadow = 'shadow';
        break;

      case 6:
        shadow = 'shadow-md';
        break;

      case 8:
        shadow = 'shadow-lg';
        break;

      case 10:
        shadow = 'shadow-xl';
        break;

      case 12:
        shadow = 'shadow-2xl';
        break;

      default:
        shadow = 'shadow-none';
    }
  }

  return /*#__PURE__*/React__default.createElement("div", {
    className: classname("max-w-sm rounded overflow-hidden bg-primary bg-opacity-20 px-4 py-2", shadow)
  }, /*#__PURE__*/React__default.createElement("div", null, /*#__PURE__*/React__default.createElement("div", {
    className: "font-bold text-lg mb-2 text-primary"
  }, heading)), /*#__PURE__*/React__default.createElement("div", null, children), actionButton && /*#__PURE__*/React__default.createElement("div", null, actionButton.button));
};

var Accordion = function Accordion(_ref) {
  var heading = _ref.heading,
      content = _ref.content;
  return /*#__PURE__*/React__default.createElement("div", {
    className: "accordion-item bg-white border border-gray-200"
  }, /*#__PURE__*/React__default.createElement("h2", {
    className: "accordion-header mb-0 font-bold",
    id: "headingOne"
  }, /*#__PURE__*/React__default.createElement("button", {
    className: "\n          accordion-button\n          relative\n          flex\n          items-center\n          w-full\n          py-4\n          px-5\n          text-base text-gray-800 text-left\n          bg-white\n          border-0\n          rounded-none\n          transition\n          focus:outline-none\n        ",
    type: "button",
    "data-bs-toggle": "collapse",
    "data-bs-target": "#collapseOne",
    "aria-expanded": "true",
    "aria-controls": "collapseOne"
  }, heading)), /*#__PURE__*/React__default.createElement("div", {
    id: "collapseOne",
    className: "accordion-collapse collapse show",
    "aria-labelledby": "headingOne",
    "data-bs-parent": "#accordionExample"
  }, /*#__PURE__*/React__default.createElement("div", {
    className: "accordion-body py-4 px-5"
  }, content)));
};

var Checkbox = function Checkbox(_ref) {
  var label = _ref.label,
      _onChange = _ref.onChange,
      size = _ref.size,
      placement = _ref.placement;
  var fontSize; // // let fontColor

  if (size) {
    switch (size) {
      case 'extrasmall':
        fontSize = 'text-xs';
        break;

      case 'small':
        fontSize = 'text-base';
        break;

      case 'medium':
        fontSize = 'text-2xl';
        break;

      case 'large':
        fontSize = 'text-6xl';
        break;
    }
  } // if(color){
  //   switch (color) {
  //     case 'primary': fontColor = 'text-primary/[0.5]'
  //       break;
  //       case 'secondary': fontColor = 'text-secondary/[0.5]'
  //       break;
  //       case 'success': fontColor = 'text-success/[0.5]'
  //       break;
  //       case 'error': fontColor = 'text-error/[0.5]'
  //       break;
  //       case 'primary2': fontColor = 'text-primary2/[0.5]'
  //       break;
  //     default:
  //       fontColor = 'text-black/[0.5]'
  //       break;
  //   }
  // }


  return /*#__PURE__*/React__default.createElement("label", {
    style: {
      color: 'rgba(0, 0, 0, 0.5)'
    },
    className: classname("md:w-2/3 block font-bold", fontSize, 'cursor-pointer')
  }, placement === 'left' && /*#__PURE__*/React__default.createElement("span", {
    className: "mr-2"
  }, label), /*#__PURE__*/React__default.createElement("input", {
    className: "leading-tight cursor-pointer",
    onChange: function onChange() {
      return _onChange;
    },
    type: "checkbox"
  }), placement === 'right' && /*#__PURE__*/React__default.createElement("span", {
    className: "ml-2"
  }, label));
};

var _excluded$8 = ["options", "disabled", "color", "size", "placeHolder", "onChange"];
var Dropdown = function Dropdown(_ref) {
  var options = _ref.options,
      disabled = _ref.disabled,
      color = _ref.color,
      size = _ref.size,
      placeHolder = _ref.placeHolder,
      onChange = _ref.onChange,
      props = _objectWithoutPropertiesLoose(_ref, _excluded$8);

  var sizeVal;
  var bgColor;

  if (size) {
    switch (size) {
      case 'extrasmall':
        sizeVal = 'py-1 px-1';
        break;

      case 'small':
        sizeVal = 'py-1 px-2';
        break;

      case 'medium':
        sizeVal = 'py-1 px-3';
        break;

      case 'large':
        sizeVal = 'py-2 px-4';
        break;

      default:
        sizeVal = 'py-2 px-5';
        break;
    }
  }

  if (color) {
    switch (color) {
      case 'primary':
        bgColor = 'bg-primary border-primary text-white';
        break;

      case 'secondary':
        bgColor = 'bg-secondary border-secondary text-white';
        break;

      case 'primary2':
        bgColor = 'bg-primary2 border-primary2 text-white';
        break;

      case 'primary3':
        bgColor = 'bg-primary3 border-primary3 text-white';
        break;

      case 'success':
        bgColor = 'bg-success border-success text-white';
        break;

      case 'error':
        bgColor = 'bg-error border-error text-white';
        break;

      case 'text':
        bgColor = 'bg-text border-text text-white';
        break;

      case 'normal':
        bgColor = 'bg-normal border-dark text-dark';
        break;

      default:
        bgColor = 'bg-primary border-primary text-white';
        break;
    }
  }

  return /*#__PURE__*/React__default.createElement("select", Object.assign({
    placeholder: placeHolder,
    disabled: disabled,
    className: classname("block cursor-pointer appearance-none border rounded leading-tight focus:outline-none", sizeVal, bgColor),
    id: "grid-state",
    onChange: onChange
  }, props), options && options.map(function (option) {
    return /*#__PURE__*/React__default.createElement("option", {
      value: option,
      key: option
    }, option);
  }));
};

var Dropzone = function Dropzone(_ref) {
  _objectDestructuringEmpty(_ref);

  return /*#__PURE__*/React__default.createElement("div", {
    className: 'w-4/12 h-2/5 p-2 text-center border-dashed border-2 border-sky-500'
  }, /*#__PURE__*/React__default.createElement("p", null, "Click to upload file  "));
};

/**
 * this component will be used a input html tag but with custom features
 */

var Inputfield = function Inputfield(_ref) {
  var varient = _ref.varient,
      fullWidth = _ref.fullWidth,
      label = _ref.label,
      disabled = _ref.disabled,
      formHelp = _ref.formHelp,
      placeholder = _ref.placeholder,
      type = _ref.type,
      size = _ref.size,
      error = _ref.error,
      onChange = _ref.onChange,
      iconPosition = _ref.iconPosition,
      iconName = _ref.iconName;
  var fullWidthClass = fullWidth ? 'w-full' : '';
  var varientClass = varient === "filled" ? 'rounded-t-lg border-b-2 border-solid' : varient === 'password' ? 'border rounded' : 'border rounded bg-gray-200';
  var sizeVal = 'py-1 px-2'; // let spanIconClass = varient === 'password' ? "\" : '';
  // let handleChange = (event) = {
  //     onChange(event.target.value)
  // }

  if (size) {
    switch (size) {
      case 'extrasmall':
        sizeVal = 'py-0.5 px-0.5';
        break;

      case 'small':
        sizeVal = 'py-1 px-2';
        break;

      case 'medium':
        sizeVal = 'py-2 px-3';
        break;

      case 'large':
        sizeVal = 'py-2.5 px-3';
        break;

      default:
        sizeVal = 'py-3 px-4';
        break;
    }
  }

  var _useState = React.useState(iconName),
      tempIconName = _useState[0],
      setIconName = _useState[1];

  var _useState2 = React.useState(type),
      inputType = _useState2[0],
      setInputType = _useState2[1];

  var handleClick = function handleClick() {
    if (tempIconName === 'eye') {
      setIconName('close-eye');
      setInputType('text');
    } else if (tempIconName === 'close-eye') {
      setIconName('eye');
      setInputType('password');
    }
  }; // const handleClick = () => {}


  return /*#__PURE__*/React__default.createElement("div", null, /*#__PURE__*/React__default.createElement("label", {
    className: "block text-gray-700 font-bold mb-2",
    htmlFor: "grid-first-name"
  }, label), /*#__PURE__*/React__default.createElement("div", {
    className: 'relative flex items-center'
  }, /*#__PURE__*/React__default.createElement("input", {
    disabled: disabled,
    className: classname("appearance-none block text-gray-700 leading-tight focus:outline-none focus:bg-white", sizeVal, fullWidthClass, varientClass),
    id: "grid-first-name",
    type: inputType,
    placeholder: placeholder,
    onChange: onChange
  }), /*#__PURE__*/React__default.createElement("span", {
    style: {
      right: '12px'
    },
    className: 'cursor-pointer ml-2 absolute',
    onClick: handleClick
  }, iconPosition === 'right' && /*#__PURE__*/React__default.createElement(Icon, {
    width: "24",
    height: "24",
    fill: "#000000",
    name: tempIconName,
    fillOpacity: 0.54
  }))), formHelp && /*#__PURE__*/React__default.createElement("p", {
    className: classname("text-xs italic ml-2 mt-0.5", error && "text-red")
  }, " ", formHelp, " "));
}; // Inputfield.defaultProps = {
//     message: 'Hello',
//     onClick: function(){ alert("Hello"); }
// };

var Modal = function Modal(_ref) {
  var heading = _ref.heading,
      body = _ref.body,
      showCloseButton = _ref.showCloseButton,
      actionButton = _ref.actionButton;
  return /*#__PURE__*/React__default.createElement("div", {
    className: "fixed z-10 overflow-y-auto top-0 w-full left-0 hidden",
    id: "modal"
  }, /*#__PURE__*/React__default.createElement("div", {
    className: "flex items-center justify-center min-height-100vh pt-4 px-4 pb-20 text-center sm:block sm:p-0"
  }, /*#__PURE__*/React__default.createElement("div", {
    className: "fixed inset-0 transition-opacity"
  }, /*#__PURE__*/React__default.createElement("div", {
    className: "absolute inset-0 bg-gray-900 opacity-75"
  })), /*#__PURE__*/React__default.createElement("span", {
    className: "hidden sm:inline-block sm:align-middle sm:h-screen"
  }, "\u200B"), /*#__PURE__*/React__default.createElement("div", {
    className: "inline-block align-center bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full",
    role: "dialog",
    "aria-modal": "true",
    "aria-labelledby": "modal-headline"
  }, heading, /*#__PURE__*/React__default.createElement("div", null, body), /*#__PURE__*/React__default.createElement("div", {
    className: "bg-gray-200 px-4 py-3 text-right"
  }, showCloseButton && /*#__PURE__*/React__default.createElement("button", {
    type: "button",
    className: "py-2 px-4 bg-gray-500 text-white rounded hover:bg-gray-700 mr-2"
  }, " Cancel"), actionButton))));
};

var Progressbar = function Progressbar(_ref) {
  var value = _ref.value,
      color = _ref.color,
      size = _ref.size;
  var height = 2;

  if (size) {
    switch (size) {
      case 'extrasmall':
        height = 1;
        break;

      case 'small':
        height = 2;
        break;

      case 'medium':
        height = 3;
        break;

      case 'large':
        height = 4;
        break;

      default:
        height = 3;
        break;
    }
  }

  return /*#__PURE__*/React__default.createElement("div", {
    className: classname("relative max-w-xl overflow-hidden rounded-2xl", "h-" + height)
  }, /*#__PURE__*/React__default.createElement("div", {
    className: "w-full h-full bg-gray-200 absolute"
  }), /*#__PURE__*/React__default.createElement("div", {
    style: {
      width: value + "%"
    },
    className: classname("h-full relative w-0", "bg-" + color)
  }));
};

var Table = function Table(_ref) {
  _objectDestructuringEmpty(_ref);

  return /*#__PURE__*/React__default.createElement("div", null);
};

var Textarea = function Textarea(_ref) {
  var color = _ref.color,
      size = _ref.size,
      variant = _ref.variant;
  var variantClass;
  var sizeClass;

  if (variant) {
    switch (variant) {
      case 'contained':
        variantClass = "bg-" + color + " text-white border text-white font-bold";
        break;

      case 'outlined':
        variantClass = "bg-transparent text-" + color + " font-semibold border border-" + color + " hover:bg-" + color + " hover:text-white border-2";
        break;
    }
  }

  if (size) {
    switch (size) {
      case 'extrasmall':
        sizeClass = "py-1 px-2";
        break;

      case 'small':
        sizeClass = "py-1 px-4";
        break;

      case 'medium':
        sizeClass = "py-2 px-4";
        break;

      case 'large':
        sizeClass = "py-3 px-6";
        break;
    }
  }

  return /*#__PURE__*/React__default.createElement("div", {
    className: classname(sizeClass, variantClass, "rounded")
  });
};

var Togglebutton = function Togglebutton(_ref) {
  var disabled = _ref.disabled,
      label = _ref.label,
      position = _ref.position;

  return /*#__PURE__*/React__default.createElement(React__default.Fragment, null, /*#__PURE__*/React__default.createElement("label", {
    htmlFor: "toogleA",
    className: "flex items-center cursor-pointer"
  }, position === 'left' && /*#__PURE__*/React__default.createElement("div", {
    className: "mr-2 text-gray-700 font-medium"
  }, label), /*#__PURE__*/React__default.createElement("div", {
    className: "relative"
  }, /*#__PURE__*/React__default.createElement("input", {
    disabled: disabled,
    id: "toogleA",
    type: "checkbox",
    className: "sr-only"
  }), /*#__PURE__*/React__default.createElement("div", {
    className: "w-5 h-2 bg-gray-400 rounded-full shadow-inner"
  }), /*#__PURE__*/React__default.createElement("div", {
    className: "dot absolute w-3 h-3 bg-white rounded-full shadow -left-1 -top-1 transition"
  })), " ", position === 'right' && /*#__PURE__*/React__default.createElement("div", {
    className: "ml-2 text-gray-700 font-medium"
  }, label), "  "));
};

var Typography = function Typography(_ref) {
  var variant = _ref.variant,
      color = _ref.color;

  switch (variant) {
    case 'h1':
      return /*#__PURE__*/React__default.createElement("h1", {
        className: "text-" + color
      });

    case 'h2':
      return /*#__PURE__*/React__default.createElement("h2", {
        className: "text-" + color
      });

    case 'h3':
      return /*#__PURE__*/React__default.createElement("h3", {
        className: "text-" + color
      });

    case 'h4':
      return /*#__PURE__*/React__default.createElement("h4", {
        className: "text-" + color
      });

    case 'h5':
      return /*#__PURE__*/React__default.createElement("h5", {
        className: "text-" + color
      });

    default:
      return /*#__PURE__*/React__default.createElement("p", null);
  }
};

var TablewithDynamicRows = function TablewithDynamicRows(_ref) {
  _objectDestructuringEmpty(_ref);

  var generateGrid = function generateGrid() {
    return [[{
      readOnly: true,
      value: 1,
      className: 'shrink'
    }, {
      readOnly: true,
      value: 1,
      className: 'shrink'
    }], [{
      readOnly: false,
      value: 1,
      className: 'shrink'
    }, {
      readOnly: false,
      value: 1,
      className: 'shrink'
    }]];
  };

  var addNewRow = function addNewRow() {};

  return /*#__PURE__*/React__default.createElement(React__default.Fragment, null, /*#__PURE__*/React__default.createElement(Datasheet, {
    data: generateGrid(),
    valueRenderer: function valueRenderer(cell) {
      return cell.value;
    },
    onContextMenu: function onContextMenu(e, cell, _i, _j) {
      return cell.readOnly ? e.preventDefault() : null;
    }
  }), /*#__PURE__*/React__default.createElement(Button, {
    onClick: function () {
      var _onClick = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
        return _regeneratorRuntime().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                return _context.abrupt("return", addNewRow());

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function onClick() {
        return _onClick.apply(this, arguments);
      }

      return onClick;
    }(),
    label: 'Add row',
    color: 'primary',
    variant: 'contained',
    size: 'small',
    textColor: ''
  }));
};

var SingleBoxWithDynamicRows = function SingleBoxWithDynamicRows(_ref) {
  _objectDestructuringEmpty(_ref);

  var handleClick = function handleClick() {};

  return /*#__PURE__*/React__default.createElement("div", {
    className: 'flex flex-row justify-evenly w-1/3'
  }, /*#__PURE__*/React__default.createElement(Inputfield, {
    label: '',
    color: 'primary',
    size: 'small',
    placeholder: '',
    formHelp: '',
    varient: '',
    fullWidth: false,
    type: 'text'
  }), /*#__PURE__*/React__default.createElement(Button, {
    className: 'mx-3',
    label: 'Add',
    color: 'primary',
    variant: 'outlined',
    size: 'extrasmall',
    textColor: '',
    onClick: function () {
      var _onClick = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
        return _regeneratorRuntime().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                return _context.abrupt("return", handleClick());

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function onClick() {
        return _onClick.apply(this, arguments);
      }

      return onClick;
    }()
  }), /*#__PURE__*/React__default.createElement(Button, {
    label: 'Remove',
    color: 'primary',
    variant: 'outlined',
    size: 'extrasmall',
    textColor: '',
    onClick: function () {
      var _onClick2 = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2() {
        return _regeneratorRuntime().wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                return _context2.abrupt("return", handleClick());

              case 1:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function onClick() {
        return _onClick2.apply(this, arguments);
      }

      return onClick;
    }()
  }));
};

var FixedTableGridMultiLevelGroupedColumnsAndSingleLevelGroupedRowsWithTabs = function FixedTableGridMultiLevelGroupedColumnsAndSingleLevelGroupedRowsWithTabs(_ref) {
  _objectDestructuringEmpty(_ref);

  return /*#__PURE__*/React__default.createElement("div", null);
};

var MegaTable = function MegaTable(_ref) {
  var column = _ref.column,
      row = _ref.row,
      _ref$noRows = _ref.noRows,
      noRows = _ref$noRows === void 0 ? 2 : _ref$noRows,
      _ref$noColumns = _ref.noColumns,
      noColumns = _ref$noColumns === void 0 ? 2 : _ref$noColumns,
      groupedRow = _ref.groupedRow,
      groupedColumn = _ref.groupedColumn,
      _ref$colGrouping = _ref.colGrouping,
      colGrouping = _ref$colGrouping === void 0 ? [[]] : _ref$colGrouping,
      rowGrouping = _ref.rowGrouping,
      cellPadding = _ref.cellPadding,
      fontSize = _ref.fontSize,
      mergedCellClass = _ref.mergedCellClass,
      cellMargin = _ref.cellMargin,
      additionalClass = _ref.additionalClass;

  var generateGrid = function generateGrid() {
    var baseData = []; // create base table

    for (var i = 1; i <= noRows; i++) {
      var rows = [];

      for (var j = 1; j <= noColumns; j++) {
        rows.push({
          readOnly: false,
          value: '',
          className: classname(cellPadding, fontSize, cellMargin, additionalClass)
        });
      }

      baseData.push(rows);
    }

    var base = [].concat(baseData); //  check for merging

    if (groupedColumn) {
      // let sumC = colGrouping?.flat().reduce((a, b) => a+b) || 1
      colGrouping == null ? void 0 : colGrouping.map(function (row, rId) {
        row.map(function (c, cId) {
          baseData[rId][cId].colSpan = c;
        });
        base[rId].splice(colGrouping[rId].length, base[rId].length);
      }); // remove unwanted cells
    }

    if (groupedRow) {
      rowGrouping == null ? void 0 : rowGrouping.map(function (row, rId) {
        return row.map(function (c, cId) {
          return baseData[cId][rId].rowSpan = c;
        });
      });
    }

    baseData = base;
    column == null ? void 0 : column.map(function (r, rId) {
      return r.map(function (_c, cId) {
        if (baseData[rId][cId]) {
          baseData[rId][cId].value = column[rId][cId];
          baseData[rId][cId].readOnly = true;
          baseData[rId][cId].className = mergedCellClass;
        }
      });
    });
    row == null ? void 0 : row.map(function (r, rId) {
      return r.map(function (_c, cId) {
        if (baseData[cId + 1][rId]) {
          baseData[cId + 1][rId].value = row[rId][cId];
          baseData[cId + 1][rId].readOnly = true;
          baseData[cId + 1][rId].className = mergedCellClass;
        }
      });
    });
    console.log(baseData);
    return baseData; //  console.log({
    //     column
    //  })
    //  return [[{
    //     readOnly: true,
    //     value:  'a',
    //     className: 'shrink'
    //  }]]
    // return [[{
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink',
    //     colSpan:2
    //   },
    //   {
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   }],
    //   [{
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   }], 
    // [{
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: false,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: false,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: false,
    //     value:  1,
    //     className: 'shrink'
    //   }]  , 
    // [{
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: false,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: false,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: false,
    //     value:  1,
    //     className: 'shrink'
    //   }]
    // ]
  };

  return /*#__PURE__*/React__default.createElement(Datasheet, {
    data: generateGrid(),
    valueRenderer: function valueRenderer(cell) {
      return cell.value;
    },
    onContextMenu: function onContextMenu(e, cell, _i, _j) {
      return cell.readOnly ? e.preventDefault() : null;
    }
  });
};

var Tabs = function Tabs(_ref) {
  _objectDestructuringEmpty(_ref);

  return /*#__PURE__*/React__default.createElement(React__default.Fragment, null, /*#__PURE__*/React__default.createElement("div", {
    className: "flex justify-center"
  }, /*#__PURE__*/React__default.createElement("div", null, /*#__PURE__*/React__default.createElement("div", {
    className: "dropdown relative"
  }, /*#__PURE__*/React__default.createElement("button", {
    className: "\n          dropdown-toggle\n          px-6\n          py-2.5\n          bg-blue-600\n          text-white\n          font-medium\n          text-xs\n          leading-tight\n          uppercase\n          rounded\n          shadow-md\n          hover:bg-blue-700 hover:shadow-lg\n          focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0\n          active:bg-blue-800 active:shadow-lg active:text-white\n          transition\n          duration-150\n          ease-in-out\n          flex\n          items-center\n          whitespace-nowrap\n        ",
    type: "button",
    id: "dropdownMenuButton1",
    "data-bs-toggle": "dropdown",
    "aria-expanded": "false"
  }, "Dropdown button", /*#__PURE__*/React__default.createElement("svg", {
    "aria-hidden": "true",
    focusable: "false",
    "data-prefix": "fas",
    "data-icon": "caret-down",
    className: "w-2 ml-2",
    role: "img",
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 0 320 512"
  }, /*#__PURE__*/React__default.createElement("path", {
    fill: "currentColor",
    d: "M31.3 192h257.3c17.8 0 26.7 21.5 14.1 34.1L174.1 354.8c-7.8 7.8-20.5 7.8-28.3 0L17.2 226.1C4.6 213.5 13.5 192 31.3 192z"
  }))), /*#__PURE__*/React__default.createElement("ul", {
    className: "\n          dropdown-menu\n          min-w-max\n          absolute\n          hidden\n          bg-white\n          text-base\n          z-50\n          float-left\n          py-2\n          list-none\n          text-left\n          rounded-lg\n          shadow-lg\n          mt-1\n          hidden\n          m-0\n          bg-clip-padding\n          border-none\n        ",
    "aria-labelledby": "dropdownMenuButton1"
  }, /*#__PURE__*/React__default.createElement("li", null, /*#__PURE__*/React__default.createElement("a", {
    className: "\n              dropdown-item\n              text-sm\n              py-2\n              px-4\n              font-normal\n              block\n              w-full\n              whitespace-nowrap\n              bg-transparent\n              text-gray-700\n              hover:bg-gray-100\n            ",
    href: "#"
  }, "Action")), /*#__PURE__*/React__default.createElement("li", null, /*#__PURE__*/React__default.createElement("a", {
    className: "\n              dropdown-item\n              text-sm\n              py-2\n              px-4\n              font-normal\n              block\n              w-full\n              whitespace-nowrap\n              bg-transparent\n              text-gray-700\n              hover:bg-gray-100\n            ",
    href: "#"
  }, "Another action")), /*#__PURE__*/React__default.createElement("li", null, /*#__PURE__*/React__default.createElement("a", {
    className: "\n              dropdown-item\n              text-sm\n              py-2\n              px-4\n              font-normal\n              block\n              w-full\n              whitespace-nowrap\n              bg-transparent\n              text-gray-700\n              hover:bg-gray-100\n            ",
    href: "#"
  }, "Something else here")))))));
};

exports.Accordion = Accordion;
exports.Button = Button;
exports.Card = Card;
exports.Checkbox = Checkbox;
exports.Dropdown = Dropdown;
exports.Dropzone = Dropzone;
exports.FixedTableGridMultiLevelGroupedColumnsAndSingleLevelGroupedRowsWithTabs = FixedTableGridMultiLevelGroupedColumnsAndSingleLevelGroupedRowsWithTabs;
exports.Icon = Icon;
exports.Inputfield = Inputfield;
exports.MegaTable = MegaTable;
exports.Modal = Modal;
exports.Progressbar = Progressbar;
exports.SingleBoxWithDynamicRows = SingleBoxWithDynamicRows;
exports.Table = Table;
exports.TablewithDynamicRows = TablewithDynamicRows;
exports.Tabs = Tabs;
exports.Textarea = Textarea;
exports.Togglebutton = Togglebutton;
exports.Typography = Typography;
//# sourceMappingURL=prism-reusable-components.cjs.development.js.map
