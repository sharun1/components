/// <reference types="react" />
export interface TogglebuttonProps {
    onChange: Function;
    color?: string;
    size?: string;
    label?: string;
    position?: string;
    disabled?: boolean;
}
export declare const Togglebutton: ({ disabled, label, position, size }: TogglebuttonProps) => JSX.Element;
