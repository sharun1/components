/// <reference types="react" />
export interface CheckboxProps {
    label: string;
    onChange: Function;
    size: string;
    placement: string;
}
export declare const Checkbox: ({ label, onChange, size, placement }: CheckboxProps) => JSX.Element;
