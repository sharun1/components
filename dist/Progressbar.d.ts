/// <reference types="react" />
export interface ProgressProps {
    label: string;
    color?: string;
    value: string;
    size?: string;
}
export declare const Progressbar: ({ value, color, size }: ProgressProps) => JSX.Element;
