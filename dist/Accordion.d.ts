/// <reference types="react" />
export interface AccordionProps {
    heading: string;
    content: any;
}
export declare const Accordion: ({ heading, content }: AccordionProps) => JSX.Element;
