import React from 'react';
export interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    label: string;
    textColor: string;
    color: string;
    variant: string;
    size: string;
    disabled?: boolean;
    buttonIcon?: any;
    iconPosition?: string;
    width?: string;
    height?: string;
    iconName?: any;
    iconColorOpacity?: any;
    iconColor?: any;
    onClick: () => {};
}
export declare const Button: ({ textColor, label, color, size, variant, disabled, iconPosition, height, width, iconName, iconColor, onClick, iconColorOpacity, ...props }: ButtonProps) => JSX.Element;
