/// <reference types="react" />
export interface ModalProps {
    heading: string;
    body: any;
    variant?: string;
    size: string;
    showCloseButton?: boolean;
    actionButton?: boolean;
}
export declare const Modal: ({ heading, body, showCloseButton, actionButton }: ModalProps) => JSX.Element;
