/// <reference types="react" />
export interface IconProps {
    width: string;
    height: string;
    fill: string;
    name: keyof typeof Icons;
    fillOpacity: number;
    onClickIcon?: () => {};
}
declare const Icons: {
    eye: ({ width, height, fill, fillOpacity, ...props }: import("./Icons/Eye").EyeProps) => JSX.Element;
    'arrow-left': ({ width, height, fill, fillOpacity, ...props }: import("./Icons/ArrowLeft").arrowLeftProps) => JSX.Element;
    'close-eye': ({ width, height, fill, fillOpacity, ...props }: import("./Icons/CloseEye").CloseEyeProps) => JSX.Element;
    pencil: ({ width, height, fill, fillOpacity, ...props }: import("./Icons/Pencil").PencilProps) => JSX.Element;
    delete: ({ width, height, fill, fillOpacity, ...props }: import("./Icons/Delete").DeleteProps) => JSX.Element;
    plus: ({ width, height, fill, fillOpacity, ...props }: import("./Icons/Plus").PlusProps) => JSX.Element;
};
export declare const Icon: ({ width, height, fill, name, fillOpacity, onClickIcon, ...props }: IconProps) => JSX.Element | null;
export {};
