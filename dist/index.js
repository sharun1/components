
'use strict'

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./prism-reusable-components.cjs.production.min.js')
} else {
  module.exports = require('./prism-reusable-components.cjs.development.js')
}
