/// <reference types="react" />
export interface TextareaProps {
    label: string;
    color: string;
    variant: string;
    size: string;
    disabled?: boolean;
}
export declare const Textarea: ({ color, size, variant }: TextareaProps) => JSX.Element;
