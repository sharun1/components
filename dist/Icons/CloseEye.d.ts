/// <reference types="react" />
export interface CloseEyeProps {
    fill: string;
    width?: string;
    height?: string;
    fillOpacity: number;
}
export declare const CloseEye: ({ width, height, fill, fillOpacity, ...props }: CloseEyeProps) => JSX.Element;
