/// <reference types="react" />
export interface PencilProps {
    fill: string;
    width?: string;
    height?: string;
    fillOpacity: number;
}
export declare const Pencil: ({ width, height, fill, fillOpacity, ...props }: PencilProps) => JSX.Element;
