/// <reference types="react" />
export interface DeleteProps {
    fill: string;
    width?: string;
    height?: string;
    fillOpacity: number;
}
export declare const Delete: ({ width, height, fill, fillOpacity, ...props }: DeleteProps) => JSX.Element;
