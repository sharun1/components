/// <reference types="react" />
export interface PlusProps {
    fill: string;
    width?: string;
    height?: string;
    fillOpacity: number;
}
export declare const Plus: ({ width, height, fill, fillOpacity, ...props }: PlusProps) => JSX.Element;
