/// <reference types="react" />
export interface arrowLeftProps {
    width: string;
    height: string;
    fill: string;
    fillOpacity: number;
}
export declare const ArrowLeft: ({ width, height, fill, fillOpacity, ...props }: arrowLeftProps) => JSX.Element;
