/// <reference types="react" />
export interface EyeProps {
    fill: string;
    width?: string;
    height?: string;
    fillOpacity: number;
}
export declare const Eye: ({ width, height, fill, fillOpacity, ...props }: EyeProps) => JSX.Element;
