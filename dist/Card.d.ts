/// <reference types="react" />
interface ActionButtonInterface {
    button: any;
    position: string;
}
export interface CardProps {
    heading: string;
    subHeading?: string;
    elevation?: number;
    actionbar?: boolean;
    children?: any;
    overlayMenu?: boolean;
    overlayMenuProps?: object;
    actionButton?: ActionButtonInterface;
}
export declare const Card: ({ heading, elevation, children, actionButton }: CardProps) => JSX.Element;
export {};
