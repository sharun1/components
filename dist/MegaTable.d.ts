/// <reference types="react" />
export interface MegaTableProps {
    column: string[][];
    row: string[][];
    noRows?: number;
    noColumns?: number;
    groupedRow?: boolean;
    groupedColumn?: boolean;
    formula?: boolean;
    cellPadding?: string;
    cellMargin?: string;
    fontSize?: string;
    colDataTypes: string[];
    colGrouping?: number[][];
    rowGrouping?: number[][];
    mergedCellClass?: string;
    additionalClass?: string;
}
export declare const MegaTable: ({ column, row, noRows, noColumns, groupedRow, groupedColumn, colGrouping, rowGrouping, cellPadding, fontSize, mergedCellClass, cellMargin, additionalClass }: MegaTableProps) => JSX.Element;
