/// <reference types="react" />
export interface TypographyProps {
    color: string;
    variant: string;
    size: string;
    fontSize: number;
}
export declare const Typography: ({ variant, color }: TypographyProps) => JSX.Element;
