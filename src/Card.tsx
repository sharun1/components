import React from 'react'
import classname from 'classnames'

interface ActionButtonInterface {
    button : any,
    position: string,
}
export interface CardProps
  {
  heading: string,
  subHeading?:  string,
  elevation?: number,
  actionbar?: boolean,
  children?: any  ,
  overlayMenu?: boolean,
  overlayMenuProps?:object,
  actionButton?: ActionButtonInterface
}

export const Card = ({ heading, elevation, children, actionButton } : CardProps ) =>{
    let shadow 
    if(elevation){
        switch(elevation){
            case 0: shadow = 'shadow-none'
             break;
            case 2: shadow = 'shadow-sm'
            break;
            case 4: shadow = 'shadow'
            break;
            case 6: shadow = 'shadow-md'
            break;
            case 8: shadow = 'shadow-lg'
            break;  
            case 10: shadow = 'shadow-xl'
            break;
            case 12: shadow = 'shadow-2xl'
            break;
            default: shadow = 'shadow-none'
        }
    }
    return <div className={classname("max-w-sm rounded overflow-hidden bg-primary bg-opacity-20 px-4 py-2", shadow)}>
    <div>
      <div className="font-bold text-lg mb-2 text-primary">{heading}</div>
    </div>
   <div>
   {
        children
    }
   </div>
   {
    actionButton && <div>
       {
        actionButton.button
       }
    </div>
   }
  </div>
} 

