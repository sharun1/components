import React from 'react'
import Datasheet from 'react-datasheet'
import { Button } from './Button'
// import './data-sheet.css'
export interface TablewithDynamicRowsProps
  {
  data:object,
}

export const TablewithDynamicRows = ({  } : TablewithDynamicRowsProps ) =>{

    const generateGrid = () => {
        return [[{
            readOnly: true,
            value:  1,
            className: 'shrink'
          },
          {
            readOnly: true,
            value:  1,
            className: 'shrink'
          }], 
        [{
            readOnly: false,
            value:  1,
            className: 'shrink'
          },
          {
            readOnly: false,
            value:  1,
            className: 'shrink'
          }]
        ]
    }

    const addNewRow = () => {
    }

    return  <>
    <Datasheet
    data={generateGrid()}
    valueRenderer={(cell: any) => cell.value}
    onContextMenu={(e, cell, _i, _j) =>
      cell.readOnly ? e.preventDefault() : null
    }
  />
  <Button onClick={async () => addNewRow()} label={'Add row'} color={'primary'} variant={'contained'} size={'small'} textColor=''/>

    </>
} 

