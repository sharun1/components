import React from 'react'
import { Button } from './Button'
import { Inputfield } from './Inputfield'
export interface SingleBoxWithDynamicRowsProps
  {
  data:object,
}

export const SingleBoxWithDynamicRows = ({  } :  SingleBoxWithDynamicRowsProps ) =>{
    let handleClick = () => {

    }

    return <div className='flex flex-row justify-evenly w-1/3' >
        <Inputfield label={''} color={'primary'} size={'small'} placeholder={''} formHelp={''} varient= {''} fullWidth={false} type={'text'}/>
        <Button className='mx-3' label={'Add'} color={'primary'} variant={'outlined'} size={'extrasmall'}  textColor='' onClick={async () => handleClick()}/>
        <Button label={'Remove'} color={'primary'} variant={'outlined'} size={'extrasmall'} textColor='' onClick={async () => handleClick()}/>
</div>
} 

