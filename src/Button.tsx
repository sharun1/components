import React from 'react'
import classname from 'classnames'
import { Icon } from './Icon'
export interface ButtonProps
  extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  label: string,
  textColor: string,
  color:  string,
  variant: string,
  size: string,
  disabled?: boolean,
  buttonIcon?:any,
  iconPosition ?: string,
  width?:string,
  height?:string,
  iconName?: any,
  iconColorOpacity ?: any,
  iconColor ?: any,
  onClick: () => {},
}

// const Icons = {
  
// }

export const Button = ({ textColor,label, color, size, variant, disabled, iconPosition, height, width, iconName, iconColor, onClick, iconColorOpacity = 1 ,...props } : ButtonProps ) => {
  let variantClass
  let sizeClass
  let colorClass
  if(variant)
  {
    switch (variant) {
      case 'contained':
        variantClass = `text-white border text-white font-bold`
        break;
      case 'outlined':
        variantClass = `bg-transparent font-semibold border border-2`
        colorClass =`text-${textColor} border-${color} hover:bg-${color}`
        break;  
      case 'text':
        colorClass =`text-${color}`
          break;
      default:
        break;
    }
  }
  if(size){
      switch (size) {
      case 'extrasmall':
        sizeClass = `py-1 px-2`
        break;
      case 'small':
        sizeClass = `py-1 px-4`
        break;  
      case 'medium':
        sizeClass = `py-2 px-4`
          break;
      case 'large':
        sizeClass = `py-3 px-6`
            break;
      default:
        break;
    }
  }
  if(color){
    switch (color) {
      case 'primary' : if(variant === 'contained'){
        colorClass = 'bg-primary hover:shadow-primary'
      }else if( variant === 'outlined'){
        colorClass =`text-${textColor} border-primary hover:shadow-primary`
      } 
        break;
      case 'secondary' : if(variant === 'contained'){
        colorClass = 'bg-secondary hover:shadow-secondary'
      }else if( variant === 'outlined'){
        colorClass =`text-${textColor} border-secondary hover:shadow-secondary`
      }
        break;
      case 'success' :if(variant === 'contained'){
        colorClass = 'bg-success hover:shadow-success'
      }else if( variant === 'outlined'){
        colorClass =`text-${textColor} border-success hover:shadow-success`
      }
        break;  
        case 'normal' :if(variant === 'contained'){
          colorClass = 'bg-normal hover:shadow-normal'
        }else if( variant === 'outlined'){
          colorClass =`text-${textColor} border-normal hover:shadow-normal`
        }
        break;  
        case 'primary2' :if(variant === 'contained'){
          colorClass = 'bg-primary2 hover:shadow-primary2'
        }else if( variant === 'outlined'){
          colorClass =`text-${textColor} border-primary2 hover:shadow-primary2`
        }
        break;  
      case 'primary3' :if(variant === 'contained'){
        colorClass = 'bg-primary3 hover:shadow-primary3'
      }else if( variant === 'outlined'){
        colorClass =`text-${textColor} border-primary3 hover:shadow-primary3`
      }
        break;  
        case 'text' :if(variant === 'contained'){
          colorClass = 'bg-text hover:shadow-text'
        }else if( variant === 'outlined'){
          colorClass =`text-${textColor} border-text hover:shadow-text`
        }
        break; 
      case 'card' :if(variant === 'contained'){
        colorClass = 'bg-card hover:shadow-card'
      }else if( variant === 'outlined'){
        colorClass =`text-${textColor} border-card hover:shadow-card`
      }
        break;
      default:colorClass = 'bg-primary'
        break;
    }
  }


    return <>
    <button  type='button' disabled={disabled} className={classname(sizeClass, variantClass, `rounded` ,'min-w-[64dp]', 'min-h-[36dp]', 'hover:shadow', 'flex','justify-center',  colorClass, width, height)} onClick={onClick} {...props}>
        <span className='mr-2'>
          {iconPosition === 'left' && <Icon width="24" height="24" fill={iconColor} name={iconName} fillOpacity={iconColorOpacity} />}
        </span>
        {label}
        <span className='ml-2'>
          {iconPosition === 'right' && <Icon width="24" height="24" fill={iconColor} name={iconName} fillOpacity={iconColorOpacity} />}
        </span>
      </button>
    </>
} 

