import React from 'react'
export interface TogglebuttonProps
  {
  onChange: Function,
  color?:  string,
  size?: string,
  label?:string,
  position?:string,
  disabled?: boolean  
}

export const Togglebutton = ({ disabled, label, position, size } : TogglebuttonProps ) =>{
    // let toggleSize = ''
    if(size){
        switch (size) {
            case 'extrasmall':
                
                break;
            case 'small':
                
                break;
            case 'medium':
                
                break;  
            case 'large':
                
                break;           
            default:
                break;
        }
    }

    return <><label
        htmlFor="toogleA"
        className="flex items-center cursor-pointer" >
             {
            position === 'left' && <div className="mr-2 text-gray-700 font-medium">
            { label }
        </div> 
        }
            <div className="relative">

            <input disabled={disabled} id="toogleA" type="checkbox" className="sr-only" />

            <div className="w-5 h-2 bg-gray-400 rounded-full shadow-inner"></div>

            <div className="dot absolute w-3 h-3 bg-white rounded-full shadow -left-1 -top-1 transition"></div>
        </div> {
            position === 'right' && <div className="ml-2 text-gray-700 font-medium">
            { label }
        </div> 
        }  </label></>

} 

