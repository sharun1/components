import React from 'react'

export interface PlusProps {
  fill: string,
  width?:string,
  height?:string,
  fillOpacity :number,
}

export const Plus = ({ width, height, fill, fillOpacity,  ...props } : PlusProps ) => (
  <svg
    width={width}
    height={height}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M18 13h-5v5c0 .55-.45 1-1 1s-1-.45-1-1v-5H6c-.55 0-1-.45-1-1s.45-1 1-1h5V6c0-.55.45-1 1-1s1 .45 1 1v5h5c.55 0 1 .45 1 1s-.45 1-1 1Z"
      fill={fill}
      fillOpacity={fillOpacity}
    />
  </svg>
)