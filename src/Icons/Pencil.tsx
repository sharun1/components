import React from 'react'
// import classname from 'classnames'
// import { Icon } from './Icon'
export interface PencilProps {
  fill: string,
  width?:string,
  height?:string,
  fillOpacity :number,
}

export const Pencil = ({ width, height, fill, fillOpacity,  ...props } : PencilProps ) => (
    <svg
      width={width}
      height={height}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M3 17.46v3.04c0 .28.22.5.5.5h3.04c.13 0 .26-.05.35-.15L17.81 9.94l-3.75-3.75L3.15 17.1c-.1.1-.15.22-.15.36ZM20.71 7.04a.996.996 0 0 0 0-1.41l-2.34-2.34a.996.996 0 0 0-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83Z"
        fill={fill}
        fillOpacity={fillOpacity}
      />
    </svg>
  )