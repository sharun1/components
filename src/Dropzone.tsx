import React from 'react'

export interface DropzoneProps
  {
  onDrop: Function,
  showFiles?: boolean,
}

export const Dropzone = ({   } : DropzoneProps ) =>{
 

    return <div className='w-4/12 h-2/5 p-2 text-center border-dashed border-2 border-sky-500' >
        <p>Click to upload file  </p>   
</div>
} 

