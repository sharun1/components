import React from 'react'
import classname from 'classnames'
export interface CheckboxProps
  {
    label: string,
    onChange:  Function,
    size: string,
    placement: string
}

export const Checkbox = ({ label, onChange, size, placement } : CheckboxProps ) =>{
    
    let fontSize 
    // // let fontColor
    if(size){
        switch(size){
            case 'extrasmall': fontSize = 'text-xs'
            break;
            case 'small': fontSize = 'text-base'
            break;
            case 'medium': fontSize = 'text-2xl'
            break;
            case 'large': fontSize = 'text-6xl'
            break;
        }
    }

    // if(color){
    //   switch (color) {
    //     case 'primary': fontColor = 'text-primary/[0.5]'
    //       break;
    //       case 'secondary': fontColor = 'text-secondary/[0.5]'
    //       break;
    //       case 'success': fontColor = 'text-success/[0.5]'
    //       break;
    //       case 'error': fontColor = 'text-error/[0.5]'
    //       break;
    //       case 'primary2': fontColor = 'text-primary2/[0.5]'
    //       break;
    //     default:
    //       fontColor = 'text-black/[0.5]'
    //       break;
    //   }
    // }

    return <label style={{ color: 'rgba(0, 0, 0, 0.5)' }} className={classname("md:w-2/3 block font-bold", fontSize, 'cursor-pointer')}>
            {
        placement === 'left' &&   <span className="mr-2">
        {
          label
        }
      </span>
    }
    <input className="leading-tight cursor-pointer" onChange={() => onChange} type="checkbox" />
    {
        placement === 'right' &&   <span className="ml-2">
        {
          label
        }
      </span>
    }
  </label>
} 

