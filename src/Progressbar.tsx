import React from 'react'
import classname from 'classnames'
export interface ProgressProps
  {
  label: string,
  color?:  string,
  value: string,
  size?: string
}

export const Progressbar = ({ value, color, size } : ProgressProps ) =>{
    let height = 2
    if(size) {
        switch (size) {
            case 'extrasmall':
                    height = 1
                break;
            case 'small':
                    height = 2
                break;
            case 'medium':
                    height = 3
                break;        
            case 'large':
                    height = 4
                break;
            default: height = 3
                break;
        }
    }
    return<div className={classname("relative max-w-xl overflow-hidden rounded-2xl", `h-${height}`)}>
    <div className="w-full h-full bg-gray-200 absolute"></div>
    <div style={{
        width: `${value}%`
    }} className={classname("h-full relative w-0" ,`bg-${color}`)} ></div>
</div>
} 

