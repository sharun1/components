import React from 'react'
import classname from 'classnames'

import { Eye } from './Icons/Eye'
import { ArrowLeft } from './Icons/ArrowLeft'
import { CloseEye } from './Icons/CloseEye'
import { Pencil } from './Icons/Pencil'
import { Delete } from './Icons/Delete'
import { Plus } from './Icons/Plus'
export interface IconProps{
    width: string,
    height: string,
    fill: string,
    name: keyof typeof Icons,
    fillOpacity: number,
    onClickIcon?: () => {},
}

const Icons =  {
    'eye': Eye,
    'arrow-left': ArrowLeft,
    'close-eye': CloseEye,
    'pencil': Pencil,
    'delete': Delete,
    'plus': Plus,
}

export const Icon = ({width = "24", height = "24", fill = "#000000", name, fillOpacity = 0.54, onClickIcon, ...props} : IconProps ) =>{
    if(Icons[name] === undefined) return null
    const IconComponent  = Icons[name]
  
    return <>
        <span onClick={onClickIcon} className={classname('cursor-pointer')}>
            <IconComponent width={width} height={height} fill={fill} fillOpacity={fillOpacity} {...props}/>
        </span>
    </>
} 

