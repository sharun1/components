import React from 'react'
import classname from 'classnames'
export interface TextareaProps
  {
  label: string,
  color:  string,
  variant: string,
  size: string,
  disabled?: boolean  
}

export const Textarea = ({   color, size, variant } : TextareaProps ) =>{
    let variantClass
  let sizeClass
  if(variant)
  {
    switch (variant) {
      case 'contained':
        variantClass = `bg-${color} text-white border text-white font-bold`
        break;
      case 'outlined':
        variantClass = `bg-transparent text-${color} font-semibold border border-${color} hover:bg-${color} hover:text-white border-2`
        break;  
      case 'text':
        
          break;
      default:
        break;
    }
  }
  if(size){
      switch (size) {
      case 'extrasmall':
        sizeClass = `py-1 px-2`
        break;
      case 'small':
        sizeClass = `py-1 px-4`
        break;  
      case 'medium':
        sizeClass = `py-2 px-4`
          break;
      case 'large':
        sizeClass = `py-3 px-6`
            break;
      default:
        break;
    }
  }

    return <div className={classname(sizeClass, variantClass, `rounded`) }>

</div>
} 

