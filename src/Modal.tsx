import React from 'react'

export interface ModalProps
  {
  heading: string,
  body:  any,
  variant?: string,
  size: string,
  showCloseButton?: boolean,
  actionButton?: boolean
}

export const Modal = ({ heading, body, showCloseButton, actionButton } : ModalProps ) =>{


    return <div className="fixed z-10 overflow-y-auto top-0 w-full left-0 hidden" id="modal">
    <div className="flex items-center justify-center min-height-100vh pt-4 px-4 pb-20 text-center sm:block sm:p-0">
      <div className="fixed inset-0 transition-opacity">
        <div className="absolute inset-0 bg-gray-900 opacity-75" />
      </div>
      <span className="hidden sm:inline-block sm:align-middle sm:h-screen">&#8203;</span>
      <div className="inline-block align-center bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full" role="dialog" aria-modal="true" aria-labelledby="modal-headline">
        {
            heading
        }
        <div>
            {
                body
            }
        </div>
        <div className="bg-gray-200 px-4 py-3 text-right">
         {
            showCloseButton &&  <button type="button" className="py-2 px-4 bg-gray-500 text-white rounded hover:bg-gray-700 mr-2" > Cancel</button>
         }
          {
            actionButton
          }
        </div>
      </div>
    </div>
  </div>
} 

