import React from 'react'
export interface AccordionProps
  {
  heading: string,
  content: any  
}

export const Accordion = ({ heading, content  } : AccordionProps ) =>{


    return  <div className="accordion-item bg-white border border-gray-200">
      <h2 className="accordion-header mb-0 font-bold" id="headingOne">
        <button className="
          accordion-button
          relative
          flex
          items-center
          w-full
          py-4
          px-5
          text-base text-gray-800 text-left
          bg-white
          border-0
          rounded-none
          transition
          focus:outline-none
        " type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true"
          aria-controls="collapseOne">
         { heading }
        </button>
      </h2>
      <div id="collapseOne" className="accordion-collapse collapse show" aria-labelledby="headingOne"
        data-bs-parent="#accordionExample">
        <div className="accordion-body py-4 px-5">
            { content }
        </div>
      </div>
    </div>
} 

