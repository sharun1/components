import classNames from 'classnames';
import React, { useState } from 'react'

import { Icon } from './Icon';

export interface InputfieldProps {
    /**
     * we have different varient(versions) of input
     */
    varient: string,
    fullWidth: boolean,
    label: string,
    color:  string,
    size: string,
    disabled?: boolean,
    onChange?: () => {},
    placeholder: string,
    type: string,
    formHelp: string,
    error?: boolean,
    iconPosition?: string,
    iconName?: any,
    value?:string
}
/**
 * this component will be used a input html tag but with custom features
 */

export const Inputfield = ({ varient,fullWidth, label, disabled , formHelp, placeholder, type, size, error, onChange, iconPosition, iconName, value, ...props } : InputfieldProps ) => {
    let fullWidthClass = fullWidth ? 'w-full' : '';
    let varientClass = varient === "filled" ? 'rounded-t-lg border-b-2 border-solid' : varient === 'password' ? 'border rounded' : 'border rounded bg-gray-200';
    let sizeVal = 'py-1 px-2'
    // let spanIconClass = varient === 'password' ? "\" : '';
    // let handleChange = (event) = {
    //     onChange(event.target.value)
    // }
    if(size){
        switch (size) {
            case 'extrasmall':
                sizeVal = 'py-0.5 px-0.5'
                break;
            case 'small':
                sizeVal = 'py-1 px-2'
                break;
            case 'medium':
                sizeVal = 'py-2 px-3'
                break;
                case 'large':
                    sizeVal = 'py-2.5 px-3'
                    break;         
            default:sizeVal = 'py-3 px-4'
                break;
        }
    }
    const [tempIconName, setIconName] = useState(iconName)
    const [inputType, setInputType] = useState(type)
    const handleClick = () => {
        if(tempIconName === 'eye') {
            setIconName('close-eye')
            setInputType('text')
        } else if(tempIconName === 'close-eye') {
            setIconName('eye');
            setInputType('password')
        }
    }
    // const handleClick = () => {}

    return <div >
        <label className="block text-gray-700 font-bold mb-2" htmlFor="grid-first-name">
            {
                label
            }
        </label>
            <div className='relative flex items-center'>
                <input disabled={disabled} className={classNames("appearance-none block text-gray-700 leading-tight focus:outline-none focus:bg-white", sizeVal, fullWidthClass, varientClass)} id="grid-first-name" type={inputType} placeholder={placeholder} onChange={onChange} value={value} {...props}/>
                <span style={{right: '12px'}} className='cursor-pointer ml-2 absolute' onClick={handleClick} >
                    {  iconPosition === 'right' && <Icon width="24" height="24" fill="#000000" name={tempIconName} fillOpacity={0.54} />}
                </span>
            </div>
            {
                formHelp &&  <p className={classNames("text-xs italic ml-2 mt-0.5", error && "text-red")}> { formHelp } </p>
            }
</div>
} 

// Inputfield.defaultProps = {
//     message: 'Hello',
//     onClick: function(){ alert("Hello"); }
// };