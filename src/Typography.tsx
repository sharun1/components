import React from 'react'
export interface TypographyProps
  {
  color:  string,
  variant: string,
  size: string,
  fontSize: number  
}

export const Typography = ({ variant, color  } : TypographyProps ) =>{

    switch (variant) {
        case 'h1':
                  return <h1 className={`text-${color}`}>
                      
                  </h1>  
            break;
        case 'h2':
                  return <h2 className={`text-${color}`}>
                      
                  </h2>  
            break;
        case 'h3':
                return <h3 className={`text-${color}`}>
                    
                </h3>
        case 'h4':
            return <h4 className={`text-${color}`}>
                
            </h4>
        case 'h5':
            return <h5 className={`text-${color}`}>
                
            </h5>                    
        default: return <p>

        </p>
            break;
    }
} 

