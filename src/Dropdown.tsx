import React from 'react'
import classname from 'classnames'
export interface DropdownProps
  {
  color:  string,
  placeHolder?: string,
  onChange?:  () => {} ,
  size: string,
  disabled?: boolean,
  options: any,
  defaultOption?: string ,
}

export const Dropdown = ({  options, disabled, color, size, placeHolder, onChange,  ...props } : DropdownProps ) =>{
    let sizeVal
    let bgColor
    if(size){
        switch (size) {
            case 'extrasmall':
                        sizeVal =  'py-1 px-1'
                break;
            case 'small':
                  sizeVal =  'py-1 px-2'
                break;
            case 'medium':
                  sizeVal =  'py-1 px-3'
                break;
            case 'large':
                  sizeVal =  'py-2 px-4'
                break;
            default: sizeVal =  'py-2 px-5'
                break;
        }
    }
    if(color){
      switch (color) {
        case 'primary': bgColor = 'bg-primary border-primary text-white' 
          break;
        case 'secondary': bgColor = 'bg-secondary border-secondary text-white' 
          break;   
        case 'primary2': bgColor = 'bg-primary2 border-primary2 text-white' 
          break;
        case 'primary3': bgColor = 'bg-primary3 border-primary3 text-white' 
          break;         
        case 'success': bgColor = 'bg-success border-success text-white' 
          break;
        case 'error': bgColor = 'bg-error border-error text-white' 
          break; 
        case 'text': bgColor = 'bg-text border-text text-white' 
          break;       
        case 'normal': bgColor = 'bg-normal border-dark text-dark' 
          break;     
        default:bgColor = 'bg-primary border-primary text-white'
          break;
      }
    }

    return <select placeholder={placeHolder} disabled={disabled} className={classname("block cursor-pointer appearance-none border rounded leading-tight focus:outline-none" ,sizeVal, bgColor)} id="grid-state" onChange={onChange} {...props}>
{
    options && options.map((option: string) =>  <option value={option} key={option}>{option}</option>
    )}

  </select>
} 

