import React from 'react'
import Datasheet from 'react-datasheet'
import classNames from 'classnames'

export interface MegaTableProps {
  column: string[][],
  row: string[][],
  noRows?: number,
  noColumns?: number,
  groupedRow?: boolean,
  groupedColumn?: boolean,
  formula?: boolean,
  cellPadding?: string,
  cellMargin?: string,
  fontSize?: string,
  colDataTypes: string[],
  colGrouping?: number[][],
  rowGrouping?: number[][],
  mergedCellClass?: string,
  additionalClass? : string
}

export const MegaTable = ({ column, row, noRows = 2, noColumns = 2, groupedRow, groupedColumn, colGrouping =[[]], rowGrouping, cellPadding, fontSize, mergedCellClass, cellMargin, additionalClass }: MegaTableProps) => {

  const generateGrid = () => {
    let baseData: any[][] = []
    // create base table
    for (let i = 1; i <= noRows; i++) {
      let rows = []
      for (let j = 1; j <= noColumns; j++) {
        rows.push({
          readOnly: false,
          value: '',
          className: classNames(cellPadding, fontSize, cellMargin, additionalClass )
        })
      }
      baseData.push(rows)
    }
    let base = [...baseData]
    //  check for merging
    if (groupedColumn) {
      // let sumC = colGrouping?.flat().reduce((a, b) => a+b) || 1
      colGrouping?.map((row, rId) => {
        row.map((c, cId) => {
        baseData[rId][cId].colSpan = c
        
      })
      base[rId].splice(colGrouping[rId].length, base[rId].length)
      })
      // remove unwanted cells

    }
    if (groupedRow) {
      rowGrouping?.map((row, rId) => row.map((c, cId) => baseData[cId][rId].rowSpan = c))
    }
 
    
   
      
      
      baseData = base
    

    column?.map((r,rId) => r.map((_c, cId) => {
      if(baseData[rId][cId]){
        baseData[rId][cId].value = column[rId][cId]
        baseData[rId][cId].readOnly = true
        baseData[rId][cId].className = mergedCellClass
      }
    }))    
    row?.map((r,rId) => r.map((_c, cId) => {
      if(baseData[cId+1][rId]){
        baseData[cId+1][rId].value = row[rId][cId]
      baseData[cId+1][rId].readOnly = true
      baseData[cId+1][rId].className = mergedCellClass
      }
    }))  
    console.log(baseData)
    return baseData
    //  console.log({
    //     column
    //  })
    //  return [[{
    //     readOnly: true,
    //     value:  'a',
    //     className: 'shrink'
    //  }]]
    // return [[{
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink',
    //     colSpan:2
    //   },
    //   {
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   }],
    //   [{
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   }], 
    // [{
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: false,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: false,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: false,
    //     value:  1,
    //     className: 'shrink'
    //   }]  , 
    // [{
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: true,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: false,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: false,
    //     value:  1,
    //     className: 'shrink'
    //   },
    //   {
    //     readOnly: false,
    //     value:  1,
    //     className: 'shrink'
    //   }]
    // ]
  }
  return <Datasheet
    data={generateGrid()}
    valueRenderer={(cell: any) => cell.value}
    onContextMenu={(e, cell, _i, _j) =>
      cell.readOnly ? e.preventDefault() : null
    }
  />
}

