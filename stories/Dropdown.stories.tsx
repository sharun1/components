import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Dropdown } from '../src';
import { listOfColors, typeOfButtons, buttonSizes } from '../utils'

const meta: Meta = {
  title: 'Input/Dropdown',
  component: Dropdown,
  argTypes:{
    onChange: {action :'clicked'},
    label:{
        control:{
            type: 'text'
        }
      },
      size:{
        control:{
            type: 'select',
            options: buttonSizes
        }
      }, 
      color:{
        control:{
            type: 'select',
            options: listOfColors
        }
      },   
      disabled:{
        control:{
            type: 'boolean'
        }
      },  
      variant:{
        control:{
            type: 'select',
            options: typeOfButtons
        }
      }, 
      options:{
        control:{
            type: 'object'
        }
      }, 
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story = (args) => <Dropdown onChange={() => {}} color={''} size={''} options={[]} {...args} />;

export const Default = Template.bind({});
Default.args = {
    label: 'Deopdown',
    options: ['test 1', 'test 2']
};