import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Togglebutton } from '../src';
import { listOfColors, typeOfButtons, buttonSizes } from '../utils'

const meta: Meta = {
  title: 'Input/Togglebutton',
  component: Togglebutton,
  argTypes:{
    onChange: {acttion :'clicked'},
    label:{
        control:{
            type: 'text'
        }
      },
      size:{
        control:{
            type: 'select',
            options: buttonSizes
        }
      }, 
      color:{
        control:{
            type: 'select',
            options: listOfColors
        }
      },   
      disabled:{
        control:{
            type: 'boolean'
        },
      },  
      position:{
        control:{
            type: 'radio',
            options: ['left', 'right']
        },
      }, 
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story = (args) => <Togglebutton  label={''} onChange={() => { } } color={''} size={''} {...args} />;

export const Default = Template.bind({});
Default.args = {
   
};