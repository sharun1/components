import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Checkbox } from '../src';
import { listOfColors, typeOfButtons, buttonSizes } from '../utils'

const meta: Meta = {
  title: 'Input/Checkbox',
  component: Checkbox,
  argTypes:{
    onChange: {action :'clicked'},
    label:{
        control:{
            type: 'text'
        }
      },
      size:{
        control:{
            type: 'select',
            options: buttonSizes
        }
      }, 
      color:{
        control:{
            type: 'select',
            options: listOfColors
        }
      }, 
      placement:{
        control:{
            type: 'radio',
            options: ['left', 'right']
        }
      }
      }, 
  parameters: {
    controls: { expanded: true }
  },
};

export default meta;

const Template: Story = (args) => <Checkbox placement={''} size='' onChange={() => { } } color='' label={''} {...args} />;

export const Default = Template.bind({});
Default.args = {
    label: 'checkbox',
    placement: 'left'
};