import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Progressbar } from '../src';
import { listOfColors, typeOfButtons, buttonSizes } from '../utils'

const meta: Meta = {
  title: 'Components/Progressbar',
  component: Progressbar,
  argTypes:{
    label:{
        control:{
            type: 'text'
        }
      },
      size:{
        control:{
            type: 'select',
            options: buttonSizes
        }
      }, 
      color:{
        control:{
            type: 'select',
            options: listOfColors
        }
      },    
      value:{
        control:{
            type: 'number'
        }
      }, 
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story = (args) => <Progressbar value={''} size='small' color='' label={''} {...args} />;

export const Default = Template.bind({});
Default.args = {
    size: 'small',
    value: 10,
    color: 'primary'
};