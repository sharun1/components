import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Tabs } from '../src'


const meta: Meta = {
  title: 'Components/Tabs',
  component: Tabs,
  argTypes:{
    onChange: {action :'clicked'},
    label:{
        control:{
            type: 'text'
        }
      }
      }, 
  parameters: {
    controls: { expanded: true }
  },
};

export default meta;

const Template: Story = (args) => <Tabs  {...args} />;

export const Default = Template.bind({});
Default.args = {
    label: 'checkbox',
    placement: 'left'
};