import React from 'react';
import { Meta, Story } from '@storybook/react';
import { TablewithDynamicRows } from '../src';
import { listOfColors, typeOfButtons, buttonSizes } from '../utils'

const meta: Meta = {
  title: 'Answer Types/TablewithDynamicRows',
  component: TablewithDynamicRows,
  argTypes:{
    data:{
        control:{
            type: 'object'
        }
      }
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story = (args) => <TablewithDynamicRows  data={[]} {...args} />;

export const Default = Template.bind({});
Default.args = {
   
};