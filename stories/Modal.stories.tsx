import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Modal } from '../src';
import { listOfColors, typeOfButtons, buttonSizes } from '../utils'

const meta: Meta = {
  title: 'Components/Modal',
  component: Modal,
  argTypes:{
    heading:{
        control:{
            type: 'text'
        }
      },
      variant:{
        control:{
            type: 'select',
            options: listOfColors
        }
      },   
      size:{
        control:{
            type: 'toggle'
        }
      }
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story = (args) => <Modal body={undefined} heading={''} variant='contained' size='small' {...args} />;

export const Default = Template.bind({});
Default.args = {
};