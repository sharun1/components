import React from 'react';
import { Meta, Story } from '@storybook/react';
import { MegaTable } from '../src';
// import { listOfColors, typeOfButtons, buttonSizes } from '../utils'

const meta: Meta = {
  title: 'Development/Megatable',
  component: MegaTable,
  argTypes:{
    noRows:{
      control:{
          type: 'number'
      }
    },
    noColumns:{
      control:{
          type: 'number'
      }
    },
    colDataTypes:{
      control:{
          type: 'object'
      }
    },
    column:{
      control:{
          type: 'object'
      }
    },
    row :{
      control:{
          type: 'object'
      }
    },
    cellPadding :{
      control:{
          type: 'string'
      }
    },
    cellMargin :{
      control:{
          type: 'string'
      }
    },
    fontSize :{
      control:{
          type: 'string'
      }
    },
    additionalClass :{
      control:{
          type: 'string'
      }
    }
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story = (args) => <MegaTable cellPadding='p-1' fontSize='text-sm' cellMargin='m-1' additionalClass={''} noRows={2} noColumns={2}  colDataTypes={[]} column={[["fgdg", "dasd"]]} row={[["Fds", "Dsfsd"]]} {...args} />;

export const Default = Template.bind({});
Default.args = {
  noRows:6,
  noColumns:6,
  column:[["", "Health insurance", "Accident insurance", "Maternity Benefits", "Paternity Benefits", "Day Care facilities"], ["Category", "Total (A)", "Number (B)", "% (B/A)", "Number (C)", "% (C / A)", "Number (D)", "% (D / A)" , "Number (E)", "% (E / A)", "Number (F) ", "% (F/ A)"]],
  row:[["Fds", "Dsfsd"]],
  cellPadding:'p-1',
  fontSize:'text-sm',
  cellMargin:'m-1'
};