import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Button } from '../src';
import { listOfColors, typeOfButtons, buttonSizes, listOfIcons } from '../utils'

const meta: Meta = {
  title: 'Components/Button',
  component: Button,
  argTypes:{
    onClick: {action :'clicked'},
    label:{
        control:{
            type: 'text'
        }
      },
      size:{
        control:{
            type: 'select',
            options: buttonSizes
        }
      }, 
      color:{
        control:{
            type: 'select',
            options: listOfColors
        }
      },   
      disabled:{
        control:{
            type: 'boolean'
        }
      },  
      variant:{
        control:{
            type: 'select',
            options: typeOfButtons
        }
      }, 
      buttonIcon:{
        control:{
            type: 'any'
        }
      }, 
      width:{
        control:{
            type: 'text'
        }
      }, 
      height:{
        control:{
            type: 'text'
        }
      }, 
      iconPosition:{
        control:{
            type: 'radio',
            options: ['left', 'right']
        }
      },  
      iconName: {
        control : {
          type: 'select',
          options: listOfIcons,
        }
      },
      iconColor: {
        control : {
          type: 'text',
        }
      },
      iconColorOpacity: {
          control: {
            type: 'number',
          }
        },
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story = (args) => <Button textColor={''} isLeftIcon={false} isRightIcon={false} onClick={function (): {} {
  throw new Error('Function not implemented.');
} } disabled={false} variant='contained' size='small' color='success' label={''} {...args} />;

export const Default = Template.bind({});
export const Secondary = Template.bind({});
export const Success = Template.bind({});
export const Error = Template.bind({});
export const Normal = Template.bind({});

Normal.args = {
    label: 'Button',
    size:'small',
    variant : 'contained',
    color: 'normal'
 };

Error.args = {
    label: 'Button',
    size:'small',
    variant : 'contained',
    color: 'error'
 };

Success.args = {
    label: 'Button',
    size:'small',
    variant : 'contained',
    color: 'success'
 };

Secondary.args = {
    label: 'Button',
    size:'small',
    variant : 'contained',
    color: 'secondary'
 };

Default.args = {
   label: 'Button',
   size:'small',
   variant : 'contained',
   color: 'primary'
};