import React from 'react';
import { Meta, Story } from '@storybook/react';
import { FixedTableGridMultiLevelGroupedColumnsAndSingleLevelGroupedRowsWithTabs } from '../src';
import { listOfColors, typeOfButtons, buttonSizes } from '../utils'

const meta: Meta = {
  title: 'Answer Types/FixedTableGridMultiLevelGroupedColumnsAndSingleLevelGroupedRowsWithTabs',
  component: FixedTableGridMultiLevelGroupedColumnsAndSingleLevelGroupedRowsWithTabs,
  argTypes:{
    data:{
        control:{
            type: 'object'
        }
      }
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story = (args) => <FixedTableGridMultiLevelGroupedColumnsAndSingleLevelGroupedRowsWithTabs  data={[]} {...args} />;

export const Default = Template.bind({});
Default.args = {
   
};