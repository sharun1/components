import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Accordion } from '../src';
// import { listOfColors, typeOfButtons, buttonSizes } from '../utils'

const meta: Meta = {
  title: 'Components/Accordion',
  component: Accordion,
  argTypes:{
    heading:{
        control:{
            type: 'text'
        }
      },
      content:{
        control:{
            type: 'text'
        }
      }  
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story = (args) => <Accordion  content={''} heading={''} {...args} />;

export const Default = Template.bind({});
Default.args = {
  
};