import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Typography } from '../src';
import { listOfColors, buttonSizes, typographyTypes } from '../utils'

const meta: Meta = {
  title: 'Components/Typography',
  component: Typography,
  argTypes:{
    onClick: {acttion :'clicked'},
      color:{
        control:{
            type: 'select',
            options: listOfColors
        }
      },    
      variant:{
        control:{
            type: 'select',
            options: typographyTypes
        }
      }, 
      fontSize:{
        control:{
            type: 'number'
        }
      },  
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story = (args) => <Typography  color={''} variant={''} size={''} fontSize={0} {...args} /> ;

export const Default = Template.bind({});
Default.args = {
};