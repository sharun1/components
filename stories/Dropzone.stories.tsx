import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Dropzone } from '../src';
import { listOfColors, typeOfButtons, buttonSizes } from '../utils'

const meta: Meta = {
  title: 'Input/Dropzone',
  component: Dropzone,
  argTypes:{
    onDrop: {acttion :'clicked'}, 
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story = (args) => <Dropzone  onDrop={() => {}} {...args} />;

export const Default = Template.bind({});
Default.args = {
   
};