import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Textarea } from '../src';
import { listOfColors, typeOfButtons, buttonSizes } from '../utils'

const meta: Meta = {
  title: 'Input/Table',
  component: Textarea,
  argTypes:{
    onClick: {acttion :'clicked'},
    label:{
        control:{
            type: 'text'
        }
      },
      size:{
        control:{
            type: 'select',
            options: buttonSizes
        }
      }, 
      color:{
        control:{
            type: 'select',
            options: listOfColors
        }
      },   
      disabled:{
        control:{
            type: 'toggle'
        }
      },  
      variant:{
        control:{
            type: 'select',
            options: typeOfButtons
        }
      }, 
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story = (args) => <Textarea variant='contained' size='small' color='' label={''} {...args} />;

export const Default = Template.bind({});
Default.args = {
    label: 'button'
};