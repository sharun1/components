import React from 'react';
import { Meta, Story } from '@storybook/react';
import { SingleBoxWithDynamicRows } from '../src';
import { listOfColors, typeOfButtons, buttonSizes } from '../utils'

const meta: Meta = {
  title: 'Answer Types/SingleBoxWithDynamicRows',
  component: SingleBoxWithDynamicRows,
  argTypes:{
    data:{
        control:{
            type: 'object'
        }
      }
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story = (args) => <SingleBoxWithDynamicRows  data={[]} {...args} />;

export const Default = Template.bind({});
Default.args = {
   
};