import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Inputfield } from '../src';
import { listOfColors, typeOfButtons, buttonSizes, listOfIcons } from '../utils'

const meta: Meta = {
  title: 'Input/Inputfield',
  component: Inputfield,
  argTypes:{
    onClick: {acttion :'clicked'},
    label:{
        control:{
            type: 'text'
        }
      },
      size:{
        control:{
            type: 'select',
            options: buttonSizes
        }
      }, 
      iconName: {
        control : {
          type: 'select',
          options: listOfIcons,
        }
      },
      color:{
        control:{
            type: 'select',
            options: listOfColors
        }
      },   
      disabled:{
        control:{
            type: 'toggle'
        }
      },  
      placeholder:{
        control:{
            type: 'text'
        }
      },   
      formHelp:{
        control:{
            type: 'text'
        }
      },   
      error:{
        control:{
            type: 'boolean'
        }
      }, 
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story = (args) => <Inputfield placeholder={''} formHelp={''}  size='small' color='' label={''} {...args} />;

export const Default = Template.bind({});
Default.args = {
    label: 'button'
};