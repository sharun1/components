import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Table } from '../src';
import { listOfColors, typeOfButtons, buttonSizes } from '../utils'

const meta: Meta = {
  title: 'Components/Table',
  component: Table,
  argTypes:{
    data:{
        control:{
            type: 'object'
        }
      }
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story = (args) => <Table  heading={[]} data={[]} {...args} />;

export const Default = Template.bind({});
Default.args = {
   
};