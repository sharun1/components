import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Icon } from '../src';
import { listOfIcons } from '../utils';


const meta: Meta = {
  title: 'Components/Icon',
  component: Icon,
  argTypes:{
    name: { control: "select", options: listOfIcons },
    onClickIcon: { action: 'clicked' },
    label:{
        control:{
            type: 'text'
        }
      },
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story = (args) => <Icon  {...args} />;

export const Default = Template.bind({});
Default.args = {
    label: 'button'
};