import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Button, Card } from '../src';
import { listOfColors, typeOfButtons, buttonSizes } from '../utils'

const meta: Meta = {
  title: 'Components/Card',
  component: Card,
  argTypes:{
    heading:{
        control:{
            type: 'text'
        }
      },
      subHeading:{
        control:{
            type: 'text'
        }
      }, 
      elevation:{
        control:{
            type: 'number'
        }
      },   
      actionbar:{
        control:{
            type: 'boolean'
        }
      },
      actionButton : {
        control:{
            type: 'any'
        }
      }
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story = (args) => <Card actionButton={{
    button: <Button label={''} color={''} variant={''} size={''} />,
    position: 'left'
}} heading='' subHeading='' {...args} > </Card>;

export const Default = Template.bind({});
Default.args = {
    heading: 'heading'
};