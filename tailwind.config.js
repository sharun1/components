/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: 'jit',
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './stories/*'],
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    fontFamily: {
      sans: ['Montserrat', 'sans-serif'],
      serif: ['Merriweather', 'serif'],
    },
    extend: {
      colors: {
        'primary': '#1E2C56',
        'success': '#88cd8b',
        'error': '#f79768',
        'secondary': '#e8e9ee',
        'normal': '#ffffff',
        'primary2': '#F96E6E',
        'primary3': '#F79666',
        'text': '#172b4d',
        'card': '#1E2B56'
      }

    },
  },
  plugins: [],
}
