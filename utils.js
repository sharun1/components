export const listOfColors = [
    'primary',
    'success',
    'error',
    'secondary',
    'normal',
    'primary2',
    'primary3',
    'text',
    'card'
]

export const typeOfButtons = [
    'contained',
    'outlined',
    'text',
]

export const buttonSizes = [
    'extrasmall',
    'small',
    'medium',
    'large'
]

export const typographyTypes = [
    'body2',
    'body1',
    'caption',
    'button',
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6','subtitle1','subtitle2', 'overline', 'inherit'
]

export const listOfIcons = [
    'eye',
    'arrow-left',
    'close-eye',
    'pencil',
    'delete',
    'plus',
]